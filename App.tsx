import React from 'react';
import { NativeBaseProvider } from 'native-base';

import Footer from './components/Footer';

export default function app() {
  return (
    <NativeBaseProvider>
      <Footer />
    </NativeBaseProvider>
  );
}