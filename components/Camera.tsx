import React, { useState } from 'react';
import { Image, ScrollView, SafeAreaView, StatusBar, Linking } from 'react-native'
import { NativeBaseProvider, Center, Button, Heading, Stack, Box, Text, VStack } from 'native-base';
import * as ImagePicker from 'expo-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const SERVER_URL = "http://192.168.43.42:8080/api/scans"

export default function Camera() {
  const [data, setData] = useState({
    cardName: "",
    releaseDate: "",
    images: { small: "", normal: "", large: "" },
    set: "",
    setName: "",
    prices: { usd: "", eur: "" },
    purchases: { tcgplayer: "", cardmarket: "" }
  })

  const styles = {
    container: {
      flex: 1,
      paddingTop: StatusBar.currentHeight + 10,
    },
    scrollView: {
      marginHorizontal: 10,
    },
    text: {
      fontSize: 42,
    },
  };

  return (
    <NativeBaseProvider>
      <SafeAreaView style={styles.container}>
        {!data.cardName ?
          <Center flex={1}>
            <VStack space={4} alignItems="center">
              <Image
                source={{
                  uri: 'https://www.dragonshield.com/storage/2021/06/MTG-Icon.png',
                }}
                style={{ width: 200, height: 200 }}
              />
              <Heading color="warning.900" size="xl">
                Magic Card Scanner
              </Heading>
              <Heading fontWeight="normal" size="sm">
                A simple application to scan your Magic cards !
              </Heading>
              <Button size="lg" variant="outline" colorScheme="red" onPress={() => takeAndUploadPhotoAsync().then((e) => {
                setData(e)
              })}>
                Take a picture
              </Button>
            </VStack>
          </Center> :
          <ScrollView style={styles.scrollView}>
            <Center flex={1}>
              <Center flex={1}>
                <Box
                  rounded="lg"
                  overflow="hidden"
                  width="80"
                  shadow={5}
                  _light={{ backgroundColor: 'gray.50' }}
                >
                  <Image
                    source={{
                      uri:
                        data.images.large,
                    }}
                    style={{ width: 320, height: 434 }}
                  />
                  <Box>
                    <Center
                      bg="warning.900"
                      position="absolute"
                      bottom={0}
                      px="3"
                      py="1.5"
                    >
                      <Text fontSize="xs" _light={{ color: 'white' }} fontWeight="700" ml="-0.5" mt="-1">~ {data.prices.eur}€ / ~ {data.prices.usd}$</Text>
                    </Center>
                  </Box>
                  <Stack p="4" space={3} style={{ paddingBottom: 10 }}>
                    <Stack space={2}>
                      <Heading size="md" ml="-1">
                        {data.cardName}
                      </Heading>
                      <Text
                        fontSize="md"
                        _light={{ color: 'warning.900' }}
                        fontWeight="400"
                        ml="-0.5"
                        mt="-1"
                      >
                        Released in <Text bold fontSize="md"
                          _light={{ color: 'warning.900' }}
                          fontWeight="700"
                          ml="-0.5"
                          mt="-1">{data.releaseDate}</Text> for the <Text bold fontSize="md"
                            _light={{ color: 'warning.900' }}
                            fontWeight="700"
                            ml="-0.5"
                            mt="-1">{data.setName} ({data.set})</Text> card set.
                      </Text>
                    </Stack>
                  </Stack>
                  <Center flex={1}>
                    <Stack direction="row" style={{ paddingBottom: 10 }}>
                      <Button.Group variant="outline">
                        <Button colorScheme="yellow" leftIcon={<Icon name="dice-d20" size={20} color="#facc15" />} onPress={() => Linking.openURL(data.purchases.tcgplayer)}>
                          TcgPlayer
                        </Button>
                        <Button colorScheme="blue" leftIcon={<MaterialIcon name="cards-outline" size={20} color="#60a5fa" />} onPress={() => Linking.openURL(data.purchases.cardmarket)}>
                          CardMarket
                        </Button>
                      </Button.Group>
                    </Stack>
                    <Stack direction="row" style={{ paddingBottom: 10 }}>
                      <Button variant="outline" colorScheme="emerald" onPress={() => takeAndUploadPhotoAsync().then((e) => {
                        setData(e)
                      })}>
                        Take a new picture
                      </Button>
                    </Stack>
                  </Center>
                </Box>
              </Center>
            </Center>
          </ScrollView>
        }

      </SafeAreaView>
    </NativeBaseProvider >
  );
}

declare global {
  interface FormDataValue {
    uri: string;
    name: string;
    type: string;
  }

  interface FormData {
    append(name: string, value: FormDataValue, fileName?: string): void;
    set(name: string, value: FormDataValue, fileName?: string): void;
  }
}

async function takeAndUploadPhotoAsync() {
  // Display the camera to the user and wait for them to take a photo or to cancel the action
  let result = await ImagePicker.launchCameraAsync({
    allowsEditing: false,
    aspect: [4, 3],
  });

  if (result.cancelled) {
    return;
  }

  // ImagePicker saves the taken photo to disk and returns a local URI to it
  let localUri = result.uri;
  let filename = localUri.split('/').pop();

  // Infer the type of the image
  let match = /\.(\w+)$/.exec(filename!);
  let type = match ? `image/${match[1]}` : `image`;

  // Upload the image using the fetch and FormData APIs
  let formData = new FormData();
  formData.append('file', { uri: localUri, name: filename!, type }, filename);

  return await fetch(SERVER_URL, {
    method: 'POST',
    body: formData,
    headers: {
      'content-type': 'multipart/form-data',
    },
  }).then((res) => res.json()).then((responseData) => {
    return responseData;
  });
}