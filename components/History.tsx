import React from 'react';
import { Container, Heading, NativeBaseProvider, Center, VStack } from 'native-base';

function ContainerComponent() {
  return (
    <Container>
      <Center flex={1}>
        <VStack space={4} alignItems="center">
          <Heading color="warning.900">
            Card History
          </Heading>
          <Heading fontWeight="normal">
            Work in Progress !
          </Heading>
        </VStack>
      </Center>
    </Container>
  );
}

export default function History() {
  return (
    <NativeBaseProvider>
      <Center flex={1}>
        <ContainerComponent />
      </Center>
    </NativeBaseProvider>
  );
}